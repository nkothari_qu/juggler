from abc import ABCMeta, abstractmethod


class NodeManager(object):

    __metaclass__ = ABCMeta

    @abstractmethod
    def add_nodes(self, count):
        raise NotImplementedError

    @abstractmethod
    def delete_nodes(self, node_ids):
        raise NotImplementedError
