from cluster_manager import ClusterManager


class AwsClusterManager(ClusterManager):

    def __init__(self, cluster_object):
        self.cluster_object = cluster_object

    def get_cluster_info(self):
        return self.cluster_object.get_cluster_info()

