from abc import ABCMeta, abstractmethod


class CloudOrchestration(object):

    __metaclass__ = ABCMeta

    @abstractmethod
    def add_nodes(self, min_count, max_count):
        raise NotImplementedError

    @abstractmethod
    def delete_nodes(self, node_ids):
        raise NotImplementedError

    @abstractmethod
    def get_cluster_info(self):
        raise NotImplementedError
