from driver import Driver
import argparse
import logging
import sys
import os
from ConfigParser import ConfigParser

logging.basicConfig(format='%(asctime)s :%(levelname)s: %(message)s', level=logging.INFO, filename='/tmp/juggler.log',
                    filemode='w')
log = logging.getLogger(__name__)

parser = argparse.ArgumentParser(description='add or remove nodes from cluster')
parser.add_argument('-c', '--config', metavar='', help='path of config file', required=True)
group = parser.add_mutually_exclusive_group(required=True)
group.add_argument('-a', '--add', type=int, metavar='', help='add nodes to the cluster')
group.add_argument('-d', '--delete', metavar='', nargs='+', help='delete mentioned nodes from the cluster')
group.add_argument('-i', '--info', action='store_true', help='get cluster info')
args = parser.parse_args()

SUCCESS = "SUCCESS"
FAILURE = "FAILURE"


def add_nodes(driver, num_nodes):

    try:
        node_manager = driver.get_node_manager()
        node_manager.add_nodes(num_nodes)
        sys.stdout.write(SUCCESS + "\n")
        sys.stdout.flush()
    except Exception as e:
        log.info('Failed to add ' + str(num_nodes) + ' nodes:' + e.message)
        sys.stderr.write(FAILURE + ": " + e.message + "\n")
        sys.stderr.flush()


def delete_nodes(driver, node_ids):

    try:
        node_manager = driver.get_node_manager()
        node_manager.delete_nodes(node_ids)
        sys.stdout.write(SUCCESS + "\n")
        sys.stdout.flush()
    except Exception as e:
        log.info('Failed to delete nodes: ' + e.message)
        sys.stderr.write(FAILURE + ": " + e.message + "\n")
        sys.stderr.flush()


def get_cluster_info(driver):

    cluster_manager = driver.get_cluster_manager()
    info = cluster_manager.get_cluster_info()
    sys.stdout.write("INFO: " + info + "\n")
    sys.stdout.flush()


def get_driver_instance(config):

    try:
        log.info('Creating driver instance')
        driver = Driver.get_instance(config)
        log.info('Successfully created driver instance')
    except Exception as e:
        log.error('Failed to create driver instance: ' + e.message)
        sys.stderr.write(FAILURE + ": " + e.message + "\n")
        sys.stderr.flush()
        return None
    return driver


def get_config(config_path):

    if not os.path.isfile(config_path):
        log.error('Config file not present')
        sys.stderr.write(FAILURE + ": " + 'Config file not present' + "\n")
        return None

    try:
        config = ConfigParser()
        config.optionxform = str
        config.read(config_path)
        return config
    except Exception as e:
        log.error('Error in config file: ' + e.message)
        sys.stderr.write(FAILURE + ": " + e.message + "\n")
        sys.stderr.flush()
        return None


def main():

    config_path = args.config
    config = get_config(config_path)

    if config is None:
        log.error('Terminating due to error in config file')
        exit(0)

    driver = get_driver_instance(config)
    if driver is None:
        log.error('Terminating due to failure to create a driver instance')
        exit(0)

    if args.add is not None:
        num_nodes = int(args.add)
        add_nodes(driver, num_nodes)
    elif args.delete is not None:
        delete_nodes(driver, args.delete)
    elif args.info:
        get_cluster_info(driver)


if __name__ == '__main__':
    main()
