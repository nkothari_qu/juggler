from cloud_orchestration_implementation.aws_cloud_orchestration import AwsCloudOrchestration
from node_manager_implementation.aws_node_manager import AwsNodeManager
from cluster_manager_implementation.aws_cluster_manager import AwsClusterManager

import threading


class Driver:

    instance = None
    lock = threading.Lock()

    def __init__(self, config):

        self.config = config
        self.cluster_object = AwsCloudOrchestration(config)
        self.node_manager = AwsNodeManager(self.cluster_object)
        self.cluster_manager = AwsClusterManager(self.cluster_object)

    def get_node_manager(self):
        return self.node_manager

    def get_cluster_manager(self):
        return self.cluster_manager

    @classmethod
    def get_instance(cls, config):

        cls.lock.acquire()

        if cls.instance is None:
            cls.instance = cls(config)

        cls.lock.release()

        return cls.instance
