from node_manager import NodeManager
import botocore.exceptions
import time
import logging

log = logging.getLogger(__name__)

server_errors = ['InternalError', 'InternalFailure', 'RequestLimitExceeded', 'ServiceUnavailable', 'Unavailable']
client_errors = ['InstanceLimitExceeded', 'InsufficientInstanceCapacity']
max_retry = 5
time_to_sleep = 1


class AwsNodeManager(NodeManager):

    def __init__(self, cluster_object):
        self.cluster_object = cluster_object

    def add_nodes(self, count):

        num_retry = 1
        min_count, max_count = count, count
        retried_when_client_error = False

        while num_retry <= max_retry:
            try:
                self.cluster_object.add_nodes(min_count, max_count)
                break
            except botocore.exceptions.ClientError as e:
                if num_retry == max_retry:
                    raise
                num_retry += 1
                log.error('Requested ' + str(count) + ' nodes could not be added: ' + e.message)
                if e.response['Error']['Code'] in server_errors:
                    time.sleep(time_to_sleep)
                elif e.response['Error']['Code'] in client_errors and not retried_when_client_error:
                    retried_when_client_error = True
                    min_count = 1
                    log.info('Trying to acquire as many nodes as possible '
                             '(min = ' + str(min_count) + ', max = ' + str(max_count))
                    time.sleep(time_to_sleep)
                else:
                    raise

    def delete_nodes(self, node_ids):

        try:
            self.cluster_object.delete_nodes(node_ids)
        except Exception as e:
            log.error('Failed to delete nodes: ' + e.message)
            raise
