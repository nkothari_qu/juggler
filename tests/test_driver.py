import unittest
import sys
import os
from mock import Mock
sys.path.insert(0, os.path.abspath('..'))

from driver import Driver


class TestDriver(unittest.TestCase):

    def test_get_instance(self):

        def side_effect(value):
            if value == 'EC2':
                return [('region_name', 'us-east-1')]
            elif value == 'INSTANCE':
                return [('ImageId', 'i-vhf')]

        config = Mock()
        config.items.side_effect = side_effect

        instance1 = Driver.get_instance(config)
        instance2 = Driver.get_instance(config)
        instance3 = Driver.get_instance(config)

        self.assertEqual(instance1, instance2)
        self.assertEqual(instance1, instance3)


if __name__ == '__main__':
    unittest.main()
