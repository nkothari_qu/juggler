import unittest
from mock import Mock
import mock
from botocore.exceptions import ClientError

import sys
import os
sys.path.insert(0, os.path.abspath('..'))

from node_manager_implementation.aws_node_manager import AwsNodeManager


class TestAwsNodeManager(unittest.TestCase):

    def setUp(self):

        self.num_nodes = 5
        aws_cloud_orchestration = Mock()
        self.cluster_object = aws_cloud_orchestration.return_value

    # Test for success case
    def test_add_nodes_success(self):

        self.cluster_object.add_nodes.return_value = []
        node_manager = AwsNodeManager(self.cluster_object)
        node_manager.add_nodes(self.num_nodes)

        # cluster_object.add_nodes is called only once, no retries
        self.assertEqual(self.cluster_object.add_nodes.call_count, 1)

        # cluster_object.add_nodes is called with parameters (min_count=num_nodes, max_count=num_nodes)
        self.cluster_object.add_nodes.assert_called_once_with(self.num_nodes, self.num_nodes)

        # No Exceptions are raised, None is returned
        self.assertIsNone(node_manager.add_nodes(self.num_nodes))

    # Test for client error case
    def test_add_nodes_client_error(self):

        response_dict = {'Error': {'Code': 'InstanceLimitExceeded'}}

        self.cluster_object.add_nodes.side_effect = ClientError(response_dict, 'error')
        node_manager = AwsNodeManager(self.cluster_object)

        with self.assertRaises(ClientError):
            node_manager.add_nodes(self.num_nodes)

        # cluster_object.add_nodes is called only twice
        self.assertEqual(self.cluster_object.add_nodes.call_count, 2)

        # first call is made with parameters (min_count=num_nodes, max_count=num_nodes)
        first_call = self.cluster_object.add_nodes.call_args_list[0]
        self.assertEqual(first_call, mock.call(self.num_nodes, self.num_nodes))

        # second call is made with parameters (min_count=1, max_count=num_nodes)
        sec_call = self.cluster_object.add_nodes.call_args_list[1]
        self.assertEqual(sec_call, mock.call(1, self. num_nodes))

        # first call causes exception, second call succeeds
        self.cluster_object.add_nodes.side_effect = [ClientError(response_dict, 'error'), []]

        # Exception is handled in first call, and second call with modified parameters succeeds
        self.assertIsNone(node_manager.add_nodes(self.num_nodes))

    # Test for server error case
    def test_add_nodes_server_error(self):

        max_retry = 5
        response_dict = {'Error': {'Code': 'InternalError'}}

        self.cluster_object.add_nodes.side_effect = ClientError(response_dict, 'error')
        node_manager = AwsNodeManager(self.cluster_object)

        with self.assertRaises(ClientError):
            node_manager.add_nodes(self.num_nodes)

        # cluster_object.add_nodes is called only 5 times
        self.assertEqual(self.cluster_object.add_nodes.call_count, max_retry)

        # cluster_object.add_nodes is called only 5 times
        self.cluster_object.add_nodes.assert_called_with(self.num_nodes, self.num_nodes)

        # first call causes exception, second call succeeds
        self.cluster_object.add_nodes.side_effect = [ClientError(response_dict, 'error'), []]

        # Exception is handled in first call, second retry succeeds
        self.assertIsNone(node_manager.add_nodes(self.num_nodes))


if __name__ == '__main__':
    unittest.main()
