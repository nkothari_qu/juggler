from cloud_orchestration import CloudOrchestration
import boto3
import logging

log = logging.getLogger(__name__)


class AwsCloudOrchestration(CloudOrchestration):

    def __init__(self, config):

        self.ec2_config_dict = dict(config.items('EC2'))
        self.instance_config_dict = dict(config.items('INSTANCE'))

        self.ec2 = boto3.resource('ec2', **self.ec2_config_dict)
        self.client = boto3.client('ec2', **self.ec2_config_dict)

    def add_nodes(self, min_count, max_count):
        instances = self.ec2.create_instances(MinCount=min_count, MaxCount=max_count, **self.instance_config_dict)
        log.info('Acquired ' + str(len(instances)) + ' nodes')

    def delete_nodes(self, node_ids):
        self.client.terminate_instances(InstanceIds=node_ids)
        log.info('Terminated ' + str(len(node_ids)) + ' nodes')

    def get_cluster_info(self):
        log.info('Info: {}')
        return '{}'


