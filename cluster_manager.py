from abc import ABCMeta, abstractmethod


class ClusterManager(object):

    __metaclass__ = ABCMeta

    @abstractmethod
    def get_cluster_info(self):
        raise NotImplementedError

